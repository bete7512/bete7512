# Hi there, I'm Bete Goshme! 👋

## GitLab Activity

- ![Gitlab Stars](https://img.shields.io/gitlab/stars/bete7512?style=social) Stars
- ![Gitlab Followers](https://img.shields.io/gitlab/followers/bete7512?style=social) Followers

## Top Languages

Here are the programming languages I frequently use in my GitLab repositories, along with their respective rankings based on GitLab's language statistics:

![Top Languages](https://gitlab-readme-stats.vercel.app/api/top-langs/?username=bete7512&layout=compact)

## Languages and Tools

I work with a variety of languages and tools, including:

- ![JavaScript](https://img.shields.io/badge/-JavaScript-F7DF1E?logo=javascript&logoColor=black&style=flat-square) JavaScript
- ![HTML5](https://img.shields.io/badge/-HTML5-E34F26?logo=html5&logoColor=white&style=flat-square) HTML5
- ![CSS3](https://img.shields.io/badge/-CSS3-1572B6?logo=css3&logoColor=white&style=flat-square) CSS3
- ![Git](https://img.shields.io/badge/-Git-F05032?logo=git&logoColor=white&style=flat-square) Git
- ![Gitlab](https://img.shields.io/badge/-Gitlab-181717?logo=gitlab&logoColor=white&style=flat-square) Gitlab
- ![VS Code](https://img.shields.io/badge/-VS_Code-007ACC?logo=visual-studio-code&logoColor=white&style=flat-square) Visual Studio Code
- ![Docker](https://img.shields.io/badge/-Docker-2496ED?logo=docker&logoColor=white&style=flat-square) Docker
- ![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?logo=graphql&logoColor=white&style=flat-square) GraphQL
- ![NGINX](https://img.shields.io/badge/-NGINX-009639?logo=nginx&logoColor=white&style=flat-square) NGINX
- ![AWS Amplify](https://img.shields.io/badge/-AWS_Amplify-FF9900?logo=amazon-aws&logoColor=white&style=flat-square) AWS Amplify
- ![AWS EC2](https://img.shields.io/badge/-AWS_EC2-232F3E?logo=amazon-aws&logoColor=white&style=flat-square) AWS EC2
- ![AWS Lambda](https://img.shields.io/badge/-AWS_Lambda-FF9900?logo=amazon-aws&logoColor=white&style=flat-square) AWS Lambda
- ![Hasura](https://img.shields.io/badge/-Hasura-FF00FF?logo=hasura&logoColor=white&style=flat-square) Hasura

Feel free to explore my repositories to learn more about the projects I'm working on!
